This project is the firmware for the Cypress CX3 processor with the image sensor module AR1630.

/cx3_firmware -> CX3 Firmware, use EZ USB Suite to compile it.
/CameraTest -> PC App to open an UVC Camera (Webcam), use QT Cretor to compile it.

More information can be found at:
https://airy3d.atlassian.net/wiki/spaces/HD/pages/476053580/AR1630+Denebola+Camera+Adapter
https://airy3d.atlassian.net/wiki/spaces/HD/pages/423002158/AR0330+Denebola+Camera+Board