#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <stdio.h>
#include <unistd.h>
#include <qthread.h>
#include <omp.h>

#define VIDEO_DEVICE    "/dev/video0"

#define WIDTH           640
#define HEIGHT          480

//#define WIDTH           1280
//#define HEIGHT          720

//#define WIDTH           1920
//#define HEIGHT          1080

//#define WIDTH           3840
//#define HEIGHT          2160

#define BUFFER          4

clock_t startx;

static double DeltaT(void) {
    clock_t stop = clock();
    double delta = (stop - startx) * 1000.0 / CLOCKS_PER_SEC;
    startx = clock();
    return delta;
}

inline uint8_t Clamp(uint32_t In){
    if (In > 255)
        return 255;
    return (uint8_t)(In);
}

class ThreadDisplayImage : public QThread {
public:
    bool *Kill;
    Ui::MainWindow *ui;
    InterfaceV4L2 *v4l2;

    ThreadDisplayImage(void) {}

    void run() override;
};

unsigned char reverse_byte(unsigned char x)
{
    static const unsigned char table[] = {
        0x00, 0x80, 0x40, 0xc0, 0x20, 0xa0, 0x60, 0xe0,
        0x10, 0x90, 0x50, 0xd0, 0x30, 0xb0, 0x70, 0xf0,
        0x08, 0x88, 0x48, 0xc8, 0x28, 0xa8, 0x68, 0xe8,
        0x18, 0x98, 0x58, 0xd8, 0x38, 0xb8, 0x78, 0xf8,
        0x04, 0x84, 0x44, 0xc4, 0x24, 0xa4, 0x64, 0xe4,
        0x14, 0x94, 0x54, 0xd4, 0x34, 0xb4, 0x74, 0xf4,
        0x0c, 0x8c, 0x4c, 0xcc, 0x2c, 0xac, 0x6c, 0xec,
        0x1c, 0x9c, 0x5c, 0xdc, 0x3c, 0xbc, 0x7c, 0xfc,
        0x02, 0x82, 0x42, 0xc2, 0x22, 0xa2, 0x62, 0xe2,
        0x12, 0x92, 0x52, 0xd2, 0x32, 0xb2, 0x72, 0xf2,
        0x0a, 0x8a, 0x4a, 0xca, 0x2a, 0xaa, 0x6a, 0xea,
        0x1a, 0x9a, 0x5a, 0xda, 0x3a, 0xba, 0x7a, 0xfa,
        0x06, 0x86, 0x46, 0xc6, 0x26, 0xa6, 0x66, 0xe6,
        0x16, 0x96, 0x56, 0xd6, 0x36, 0xb6, 0x76, 0xf6,
        0x0e, 0x8e, 0x4e, 0xce, 0x2e, 0xae, 0x6e, 0xee,
        0x1e, 0x9e, 0x5e, 0xde, 0x3e, 0xbe, 0x7e, 0xfe,
        0x01, 0x81, 0x41, 0xc1, 0x21, 0xa1, 0x61, 0xe1,
        0x11, 0x91, 0x51, 0xd1, 0x31, 0xb1, 0x71, 0xf1,
        0x09, 0x89, 0x49, 0xc9, 0x29, 0xa9, 0x69, 0xe9,
        0x19, 0x99, 0x59, 0xd9, 0x39, 0xb9, 0x79, 0xf9,
        0x05, 0x85, 0x45, 0xc5, 0x25, 0xa5, 0x65, 0xe5,
        0x15, 0x95, 0x55, 0xd5, 0x35, 0xb5, 0x75, 0xf5,
        0x0d, 0x8d, 0x4d, 0xcd, 0x2d, 0xad, 0x6d, 0xed,
        0x1d, 0x9d, 0x5d, 0xdd, 0x3d, 0xbd, 0x7d, 0xfd,
        0x03, 0x83, 0x43, 0xc3, 0x23, 0xa3, 0x63, 0xe3,
        0x13, 0x93, 0x53, 0xd3, 0x33, 0xb3, 0x73, 0xf3,
        0x0b, 0x8b, 0x4b, 0xcb, 0x2b, 0xab, 0x6b, 0xeb,
        0x1b, 0x9b, 0x5b, 0xdb, 0x3b, 0xbb, 0x7b, 0xfb,
        0x07, 0x87, 0x47, 0xc7, 0x27, 0xa7, 0x67, 0xe7,
        0x17, 0x97, 0x57, 0xd7, 0x37, 0xb7, 0x77, 0xf7,
        0x0f, 0x8f, 0x4f, 0xcf, 0x2f, 0xaf, 0x6f, 0xef,
        0x1f, 0x9f, 0x5f, 0xdf, 0x3f, 0xbf, 0x7f, 0xff,
    };
    return table[x];
}

#define SAVE_EVERY_FRAME 0

void ThreadDisplayImage::run() {
    void * buffer;
    uint32_t size;

    if (v4l2->SetFmt(V4L2_PIX_FMT_YUYV))
        printf("Set Pixel Format\n");
    if (v4l2->SetFps(60))
        printf("Set FPS rate\n");
    if (v4l2->MemMap())
        printf("Map memory buffers\n");
    if (v4l2->StreamOn())
        printf("Start to stream\n");

    uint32_t width;
    uint32_t height;
    v4l2->GetCurrentResolution(&width, &height);
    ui->label_2->setGeometry(10, 60, width, height);

    uint8_t * pi = (uint8_t *)malloc(width*height*sizeof(uint8_t));
    omp_set_num_threads(4);

    while (!*this->Kill) {
        if (v4l2->GetFrame(&buffer, &size)){
            const uint8_t *p = (uint8_t*)buffer;
#if SAVE_EVERY_FRAME
            static int yuv_index = 0;
            char yuvifle[100];
            sprintf(yuvifle, "ar1630VGA-%d.data", yuv_index);
            FILE *fp = fopen(yuvifle, "wb");
            fwrite(buffer, width*height, 2, fp);
            fclose(fp);
            yuv_index++;
#endif
            //DeltaT();
/*#pragma omp parallel
{
            int numThreads = omp_get_num_threads();
            unsigned int threadId = omp_get_thread_num();
            printf("Start thread = %d \n", threadId);
            int threadPixelOffset = width / numThreads;
            unsigned int Begin = threadId*threadPixelOffset;
            unsigned int End = threadId * threadPixelOffset + threadPixelOffset;
            for(uint32_t x=Begin; x<End; x=x+1){
            //#pragma omp for schedule(static)*/
            /*for(int x=0; x<width; x=x+1){
                for(uint32_t y=0; y<height; y=y+1){
                    uint8_t Y  = *(p + x + y*width)>>2;
                    Y = Clamp(Y);
                    *(pi + x + y*width) = Y;
                }
            }*/
//}
            //printf("Time to copy buffer from v4l2 = %f ms\n", DeltaT());
            QImage image(p, width, height, QImage::Format_Grayscale8);
            ui->label_2->setPixmap(QPixmap::fromImage(image));
            //printf("QT Time to copy and display the image = %f ms\n", DeltaT());
        }
    }

    free(pi);

    if (v4l2->StreamOff())
        printf("Stop the stream\n");
    if (v4l2->MemUnmap())
        printf("Unmap memory buffers\n");

    printf("Thread DisplayImage finished!\n");
}

ThreadDisplayImage *thrd_DisplayImage;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setWindowTitle("Webcam Test");

    const char * video_device = VIDEO_DEVICE;
    if (v4l2.Open(video_device, BUFFER, WIDTH, HEIGHT)){
        printf("Opened %s\n", video_device);
        char * camera_name;
        if (v4l2.QueryCap(&camera_name)){
            ui->label->setText("Webcam = " + QString::fromUtf8(camera_name));
        }
        v4l2.SetFmt(V4L2_PIX_FMT_YUYV);
        CameraStr info;
        if (v4l2.GetFmt(&info.VideoFormat, &info.Width, &info.Height)){
            char videoformat[8] = {0};
            uint32_t pixelformat = info.VideoFormat;
            sprintf(videoformat, "%c%c%c%c",
                     pixelformat & 0xFF,
                    (pixelformat >> 8) & 0xFF,
                    (pixelformat >> 16) & 0xFF,
                    (pixelformat >> 24) & 0xFF);
            uint32_t width, height;
            for (int i=0; i<20; i++){
                if (v4l2.GetResolutions(info.VideoFormat, i, &width, &height)){
                    height *= 2;
                    printf("Resolution %d  = %d x %d\n", i+1, width, height);
                    QString text = "Resolution (" + QString::number(width) + ", " + QString::number(height);
                    text += "), Video Format " + QString::fromUtf8(videoformat);
                    printf("%s\n", text.toUtf8().constData());
                    info.Width = width;
                    info.Height = height;
                    CameraInfo.append(info);
                    ui->comboBox->addItem(text);
                }else{
                    printf("%d Resolutions on this device\n", i);
                    break;
                }
            }
        }
        v4l2.SetFps(60);
        v4l2.MemMap();
        v4l2.StreamOn();
    }

    connect(ui->comboBox, SIGNAL(currentIndexChanged(int)), this, SLOT (on_comboBox_IndexChanged(int)));
    connect(ui->pushButton, SIGNAL(released()), this, SLOT(on_pushbutton_released()) );

    Kill = false;

    usleep(50000);

    thrd_DisplayImage = new ThreadDisplayImage;
    thrd_DisplayImage->Kill = &Kill;
    thrd_DisplayImage->ui = ui;
    thrd_DisplayImage->v4l2 = &v4l2;
    thrd_DisplayImage->start();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_comboBox_IndexChanged(int idx)
{
    printf("Idx = %d, Resolution (%d, %d)\n", idx, CameraInfo.at(idx).Width, CameraInfo.at(idx).Height);

    Kill = true;
    usleep(100000);

    if (v4l2.Close())
        printf("Closed video device\n");

    const char * video_device = VIDEO_DEVICE;
    if (v4l2.Open(video_device, BUFFER, CameraInfo.at(idx).Width, CameraInfo.at(idx).Height)){
        printf("Opened %s\n", video_device);

        Kill = false;
        usleep(10000);

        thrd_DisplayImage = new ThreadDisplayImage;
        thrd_DisplayImage->Kill = &Kill;
        thrd_DisplayImage->ui = ui;
        thrd_DisplayImage->v4l2 = &v4l2;
        thrd_DisplayImage->start();
    }
}

void MainWindow::handleFrame(QImage frame){
    //ui->viewfinder = frame;
    if (!frame.isNull()){
        m_dialog->setImage(frame);
    } else {
        printf("isNull %d, \n", frame.isNull());
    }
}

void MainWindow::on_pushbutton_released(){
/*    void * buffer;
    uint32_t size;

    if (v4l2.GetFrame(&buffer, &size)){
        printf("Get a frame of %d bytes\n", size);
        ui->label_2->setGeometry(10, 60, WIDTH, HEIGHT);
        QImage image(WIDTH, HEIGHT, QImage::Format_RGB888);
        uint8_t Y;
        const uint16_t *p = (uint16_t*)buffer;
        uint32_t width = image.width();
        for(int x=0; x<image.width(); x=x+1){
            for(int y=0; y<image.height(); y=y+1){
                Y  = *(p + x + y*width);
                //Y = Clamp(Y);
                image.setPixel(x, y, qRgb(Y, Y, Y));
            }
        }
        ui->label_2->setPixmap(QPixmap::fromImage(image));
    }*/
}

void MainWindow::closeEvent(QCloseEvent *event){
    Kill = true;
    usleep(100000);
    if (v4l2.StreamOff())
        printf("Stop the stream\n");
    if (v4l2.MemUnmap())
        printf("Unmap memory buffers\n");
    if (v4l2.Close())
        printf("Closed video device\n");
}
