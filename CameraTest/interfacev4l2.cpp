#include "interfacev4l2.h"

#include <errno.h>
#include <fcntl.h>
#include <inttypes.h>
#include <linux/videodev2.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

InterfaceV4L2::InterfaceV4L2()
{
    this->fd = -1;
    this->isOpen = false;
}

InterfaceV4L2::~InterfaceV4L2()
{
}

bool InterfaceV4L2::Open(const char * device, uint8_t buf_num, uint32_t image_width, uint32_t image_height)
{
    this->device = device;
    this->BUF_NUM = buf_num;
    this->IMAGE_WIDTH = image_width;
    this->IMAGE_HEIGHT = image_height;
    struct stat st;
    memset(&st, 0, sizeof(st));
    if (stat(device, &st) == -1) {
      printf("stat error!\n");
      return false;
    }
    if (!S_ISCHR(st.st_mode)) {
      fprintf(stderr, "%s is no character device\n", device);
      return false;
    } else
      printf("%s is a character device\n", device);
    this->fd = open(this->device, O_RDWR | O_NONBLOCK, 0); //"/dev/video0"
    if (this->fd == -1){ // couldn't find capture device
        perror("Opening Video device");
        return false;
    }
    this->isOpen = true;
    return true;
}

bool InterfaceV4L2::GetCurrentResolution(uint32_t *width, uint32_t *height){
    if (this->isOpen){
        *width = IMAGE_WIDTH;
        *height = IMAGE_HEIGHT;
        return true;
    }
    return false;
}

bool InterfaceV4L2::Close()
{
    if (this->isOpen){
        if (close(this->fd) == 0){
            this->isOpen = false;
            this->fd = -1;
            return true;
        }
    }
    return false;
}

bool InterfaceV4L2::QueryCap(char ** camera_name)
{
    if (this->isOpen){
        static struct v4l2_capability caps;
        if (ioctl(this->fd, VIDIOC_QUERYCAP, &caps) == -1){
            perror("Querying Capabilites");
            return false;
        }
        printf("driver:\t\t%s\n", caps.driver);
        printf("card:\t\t%s\n", caps.card);
        printf("bus_info:\t%s\n", caps.bus_info);
        printf("version:\t%d\n", caps.version);
        printf("capabilities:\t%x\n", caps.capabilities);
        if ((caps.capabilities & V4L2_CAP_VIDEO_CAPTURE) == V4L2_CAP_VIDEO_CAPTURE) {
          printf("Device %s: supports capture.\n", this->device);
        }
        if ((caps.capabilities & V4L2_CAP_STREAMING) == V4L2_CAP_STREAMING) {
          printf("Device %s: supports streaming.\n", this->device);
        }
        struct v4l2_fmtdesc fmtdesc;
        fmtdesc.index = 0;
        fmtdesc.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        printf("\033[31mSupport format:\n\033[0m");
        while (ioctl(fd, VIDIOC_ENUM_FMT, &fmtdesc) != -1) {
          printf("\033[31m\t%d.%s\n\033[0m", fmtdesc.index + 1, fmtdesc.description);
          fmtdesc.index++;
        }
        *camera_name = (char *)caps.card;
        return true;
    }
    return false;
}

bool InterfaceV4L2::SetFmt(uint32_t pfmt){
    if (this->isOpen){
        struct v4l2_format fmt;
        fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        fmt.fmt.pix.pixelformat = pfmt;
        fmt.fmt.pix.height = IMAGE_HEIGHT;
        fmt.fmt.pix.width = IMAGE_WIDTH;
        fmt.fmt.pix.field = V4L2_FIELD_INTERLACED;
        if (ioctl(fd, VIDIOC_S_FMT, &fmt) == -1) {
          printf("Unable to set format\n");
          return false;
        }
        return true;
    }
    return false;
}

bool InterfaceV4L2::GetFmt(uint32_t *pfmt, uint32_t *width, uint32_t *height){
    if (this->isOpen){
        struct v4l2_format fmt;
        memset(&fmt, 0, sizeof(fmt));
        fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        if (ioctl(fd, VIDIOC_G_FMT, &fmt) == -1) {
          fprintf(stderr, "Unable to get format\n");
          return false;
        }
        printf("\033[33mpix.pixelformat:\t%c%c%c%c\n\033[0m",
               fmt.fmt.pix.pixelformat & 0xFF, (fmt.fmt.pix.pixelformat >> 8) & 0xFF,
               (fmt.fmt.pix.pixelformat >> 16) & 0xFF,
               (fmt.fmt.pix.pixelformat >> 24) & 0xFF);
        printf("pix.height:\t\t%d\n", fmt.fmt.pix.height);
        printf("pix.width:\t\t%d\n", fmt.fmt.pix.width);
        printf("pix.field:\t\t%d\n", fmt.fmt.pix.field);
        *pfmt = fmt.fmt.pix.pixelformat;
        *width = fmt.fmt.pix.width;
        *height = fmt.fmt.pix.height;
        return true;
    }
    return false;
}

bool InterfaceV4L2::GetResolutions(uint32_t pixelformat, uint8_t index, uint32_t *width, uint32_t *height){
    if (this->isOpen){
        v4l2_frmsizeenum frm;
        frm.pixel_format = pixelformat;
        frm.index = index;
        if (ioctl(fd, VIDIOC_ENUM_FRAMESIZES, &frm) == 0){
            *width = frm.discrete.width;
            *height = frm.discrete.height;
            return true;
        }
        *width = 0;
        *height = 0;
    }
    return false;
}

bool InterfaceV4L2::SetFps(uint32_t fps){
    if (this->isOpen){
        struct v4l2_streamparm setfps;
        memset(&setfps, 0, sizeof(setfps));
        setfps.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        setfps.parm.capture.timeperframe.numerator = 1;
        setfps.parm.capture.timeperframe.denominator = fps;
        if (ioctl(fd, VIDIOC_S_PARM, &setfps) == -1) {
          // no fatal error ,just put err msg
          fprintf(stderr, "Unable to set framerate\n");
          return false;
        }
        return true;
    }
    return false;
}

bool InterfaceV4L2::MemMap(){
    if (this->isOpen){
        struct v4l2_requestbuffers req;
        req.count = BUF_NUM;
        req.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        req.memory = V4L2_MEMORY_MMAP;
        if (ioctl(fd, VIDIOC_REQBUFS, &req) == -1) {
          printf("request for buffers error\n");
          return false;
        }
        // mmap for buffers
        v4l2_ubuffers = (v4l2_ubuffer*)(malloc(req.count * sizeof(struct v4l2_ubuffer)));
        if (!v4l2_ubuffers) {
          printf("Out of memory\n");
          return false;
        }
        struct v4l2_buffer buf;
        unsigned int n_buffers;
        for (n_buffers = 0; n_buffers < req.count; n_buffers++) {
          buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
          buf.memory = V4L2_MEMORY_MMAP;
          buf.index = n_buffers;
          // query buffers
          if (ioctl(fd, VIDIOC_QUERYBUF, &buf) == -1) {
            printf("query buffer error\n");
            return false;
          }
          v4l2_ubuffers[n_buffers].length = buf.length;
          // map 4 buffers in driver space to usersapce
          v4l2_ubuffers[n_buffers].start = mmap(NULL, buf.length, PROT_READ | PROT_WRITE, MAP_SHARED, fd, buf.m.offset);
          printf("buffer offset:%d\tlength:%d\n", buf.m.offset, buf.length);
          /**
            *  output:
            *  buffer offset:0	length:614400
            *  buffer offset:614400	length:614400
            *  buffer offset:1228800	length:614400
            *  buffer offset:1843200	length:614400
            *
            *  explanation：saved in YUV422 format，a pixel needs 2 byte storage in
            *  average，as our image size is 640*480. 640*480*2=614400
          */
          if (v4l2_ubuffers[n_buffers].start == MAP_FAILED) {
            printf("buffer map error %u\n", n_buffers);
            return false;
          }
        }
        return true;
    }
    return false;
}

bool InterfaceV4L2::MemUnmap(){
    if (this->isOpen){
        int i;
        for (i = 0; i < BUF_NUM; i++) {
            if (munmap(v4l2_ubuffers[i].start, v4l2_ubuffers[i].length) == -1) {
                printf("munmap failure %d\n", i);
                return false;
            }
        }
        return true;
    }
    return false;
}

bool InterfaceV4L2::StreamOn(){
    if (this->isOpen){
        // queue in the four buffers allocated by VIDIOC_REQBUFS, pretty like water
        // filling a bottle in turn
        struct v4l2_buffer buf;
        buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        buf.memory = V4L2_MEMORY_MMAP;
        unsigned int n_buffers;
        for (n_buffers = 0; n_buffers < BUF_NUM; n_buffers++) {
          buf.index = n_buffers;
          if (ioctl(fd, VIDIOC_QBUF, &buf) == -1) {
            printf("queue buffer failed\n");
            return false;
          }
        }

        enum v4l2_buf_type type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        if(ioctl(fd, VIDIOC_STREAMON, &type) == -1) {
          printf("stream on failed\n");
          return false;
        }
        return true;
    }
    return false;
}

bool InterfaceV4L2::StreamOff(){
    if (this->isOpen){
        enum v4l2_buf_type type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        if (ioctl(fd, VIDIOC_STREAMOFF, &type) == -1) {
          printf("stream off failed\n");
          return -1;
        }
        printf("stream is off\n");
        return true;
    }
    return false;
}

bool InterfaceV4L2::GetFrame(void **frame, uint32_t *size){
    if (this->isOpen){
        fd_set fds;
        struct v4l2_buffer buf;

        int ret;
    RETURN:
        FD_ZERO(&fds);
        FD_SET(fd, &fds);
        struct timeval tv = {.tv_sec = 1, .tv_usec = 0};
        ret = select(fd + 1, &fds, NULL, NULL, &tv);
        if (-1 == ret) {
          fprintf(stderr, "select error\n");
          return false;
        } else if (0 == ret) {
          fprintf(stderr, "timeout waiting for frame\n");
          goto RETURN;
        }
        if (FD_ISSET(fd, &fds)) {
          memset(&buf, 0, sizeof(buf));
          buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
          buf.memory = V4L2_MEMORY_MMAP;
          if (-1 == ioctl(fd, VIDIOC_DQBUF, &buf)) {
            fprintf(stderr, "VIDIOC_DQBUF failure\n");
            return false;
          }
          //printf("deque buffer %d\n", buf.index);

          *size = buf.length;
          *frame = v4l2_ubuffers[buf.index].start;

    //      if (handler)
    //        (*handler)(v4l2_ubuffers[buf.index].start,
    //                   v4l2_ubuffers[buf.index].length);

          buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
          buf.memory = V4L2_MEMORY_MMAP;
          if (-1 == ioctl(fd, VIDIOC_QBUF, &buf)) {
            fprintf(stderr, "VIDIOC_QBUF failure\n");
            return false;
          }
          //printf("queue buffer %d\n", buf.index);
        }
        return true;
    }
    return false;
}
