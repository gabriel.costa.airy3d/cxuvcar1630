#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QDialog>
#include <QLabel>
#include <QHBoxLayout>

#include "interfacev4l2.h"

typedef struct Camera_Struct {
   uint32_t VideoFormat;
   uint32_t Width;
   uint32_t Height;
} CameraStr;

namespace Ui {
    class MainWindow;
}

class Dialog : public QDialog
{
public:
    Dialog(QWidget *parent=0): QDialog(parent)
    {
        label = new QLabel(this);
        QHBoxLayout *layout = new QHBoxLayout;
        layout->addWidget(label);
        setLayout(layout);
    }

    void setImage(const QImage& image) {
        label->setPixmap(QPixmap::fromImage(image));
    }

    QLabel* label { 0 };
};

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

protected:
    void closeEvent(QCloseEvent *event) override;

private slots:
    void on_comboBox_IndexChanged(int idx);
    void handleFrame(QImage frame);
    void on_pushbutton_released();

private:
    Ui::MainWindow *ui;

    QList<Camera_Struct> CameraInfo;
    Dialog* m_dialog;
    InterfaceV4L2 v4l2;
    bool Kill;
};

#endif // MAINWINDOW_H
