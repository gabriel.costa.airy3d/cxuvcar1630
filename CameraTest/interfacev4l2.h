#ifndef INTERFACEV4L2_H
#define INTERFACEV4L2_H

#include <linux/videodev2.h>
#include <stdint.h>
#include <pthread.h>

struct v4l2_ubuffer {
  void *start;
  unsigned int length;
};

class InterfaceV4L2
{
    private:
        uint8_t BUF_NUM;

        uint32_t IMAGE_WIDTH;
        uint32_t IMAGE_HEIGHT;

        int fd;
        const char * device;
        struct v4l2_ubuffer *v4l2_ubuffers;
        bool isOpen;

    public:
        InterfaceV4L2();
        ~InterfaceV4L2();

        bool Open(const char * device, uint8_t buf_num, uint32_t image_width, uint32_t image_height);
        bool GetCurrentResolution(uint32_t *width, uint32_t *height);
        bool Close();
        bool QueryCap(char ** camera_name);
        bool SetFmt(uint32_t pfmt);
        bool GetFmt(uint32_t *pfmt, uint32_t *width, uint32_t *height);
        bool GetResolutions(uint32_t pixelformat, uint8_t index, uint32_t *width, uint32_t *height);
        bool SetFps(uint32_t fps);
        bool MemMap();
        bool MemUnmap();
        bool StreamOn();
        bool StreamOff();
        bool GetFrame(void **frame, uint32_t *size);
};

#endif // INTERFACEV4L2_H
