
/*
## Cypress CX3 configuration settings file
## ===========================
##
##  Copyright Cypress Semiconductor Corporation, 2013,
##  All Rights Reserved
##  UNPUBLISHED, LICENSED SOFTWARE.
##
##  CONFIDENTIAL AND PROPRIETARY INFORMATION
##  WHICH IS THE PROPERTY OF CYPRESS.
##
##  Use of this file is governed
##  by the license agreement included in the file
##
##     <install>/license/license.txt
##
##  where <install> is the Cypress software
##  installation root directory path.
##
## ===========================
*/

#include "cyu3mipicsi.h"


/* AR1630_YUY2_4K :  */
CyU3PMipicsiCfg_t AR1630_YUY2_4K =  
{
    CY_U3P_CSI_DF_YUV422_8_2,  /* CyU3PMipicsiDataFormat_t dataFormat */
    4,                          /* uint8_t numDataLanes */
    2,				/* uint8_t pllPrd */
    123,			/* uint16_t pllFbd */
    CY_U3P_CSI_PLL_FRS_500_1000M, /* CyU3PMipicsiPllClkFrs_t pllFrs */  
    CY_U3P_CSI_PLL_CLK_DIV_8,	/* CyU3PMipicsiPllClkDiv_t csiRxClkDiv */
    CY_U3P_CSI_PLL_CLK_DIV_8,	/* CyU3PMipicsiPllClkDiv_t parClkDiv */
    0,		                /* uint16_t mClkCtl */
    CY_U3P_CSI_PLL_CLK_DIV_2,	/* CyU3PMipicsiPllClkDiv_t mClkRefDiv */
    3840,		        /* uint16_t hResolution */
    64	                        /* uint16_t fifoDelay */
};



/* AR1630_YUY2_1080p :  */
CyU3PMipicsiCfg_t AR1630_YUY2_1080p =  
{
    CY_U3P_CSI_DF_YUV422_8_2,  /* CyU3PMipicsiDataFormat_t dataFormat */
    4,                          /* uint8_t numDataLanes */
    2,				/* uint8_t pllPrd */
    123,			/* uint16_t pllFbd */
    CY_U3P_CSI_PLL_FRS_500_1000M, /* CyU3PMipicsiPllClkFrs_t pllFrs */  
    CY_U3P_CSI_PLL_CLK_DIV_8,	/* CyU3PMipicsiPllClkDiv_t csiRxClkDiv */
    CY_U3P_CSI_PLL_CLK_DIV_8,	/* CyU3PMipicsiPllClkDiv_t parClkDiv */
    0,		                /* uint16_t mClkCtl */
    CY_U3P_CSI_PLL_CLK_DIV_2,	/* CyU3PMipicsiPllClkDiv_t mClkRefDiv */
    1920,		        /* uint16_t hResolution */
    64	                        /* uint16_t fifoDelay */
};



/* AR1630_YUY2_720p :  */
CyU3PMipicsiCfg_t AR1630_YUY2_720p =  
{
    CY_U3P_CSI_DF_YUV422_8_2,  /* CyU3PMipicsiDataFormat_t dataFormat */
    4,                          /* uint8_t numDataLanes */
    2,				/* uint8_t pllPrd */
    123,			/* uint16_t pllFbd */
    CY_U3P_CSI_PLL_FRS_500_1000M, /* CyU3PMipicsiPllClkFrs_t pllFrs */  
    CY_U3P_CSI_PLL_CLK_DIV_8,	/* CyU3PMipicsiPllClkDiv_t csiRxClkDiv */
    CY_U3P_CSI_PLL_CLK_DIV_8,	/* CyU3PMipicsiPllClkDiv_t parClkDiv */
    0,		                /* uint16_t mClkCtl */
    CY_U3P_CSI_PLL_CLK_DIV_2,	/* CyU3PMipicsiPllClkDiv_t mClkRefDiv */
    1280,		        /* uint16_t hResolution */
    64	                        /* uint16_t fifoDelay */
};



/* AR1630_YUY2_VGA :  */
CyU3PMipicsiCfg_t AR1630_YUY2_VGA =  
{
    CY_U3P_CSI_DF_YUV422_8_2,  /* CyU3PMipicsiDataFormat_t dataFormat */
    4,                          /* uint8_t numDataLanes */
    2,				/* uint8_t pllPrd */
    123,			/* uint16_t pllFbd */
    CY_U3P_CSI_PLL_FRS_250_500M, /* CyU3PMipicsiPllClkFrs_t pllFrs */  
    CY_U3P_CSI_PLL_CLK_DIV_4,	/* CyU3PMipicsiPllClkDiv_t csiRxClkDiv */
    CY_U3P_CSI_PLL_CLK_DIV_4,	/* CyU3PMipicsiPllClkDiv_t parClkDiv */
    0,		                /* uint16_t mClkCtl */
    CY_U3P_CSI_PLL_CLK_DIV_2,	/* CyU3PMipicsiPllClkDiv_t mClkRefDiv */
    640,		        /* uint16_t hResolution */
    64	                        /* uint16_t fifoDelay */
};

/* [ ] */
