
/*
 ## Cypress Image Sensor Routines for XXXX sensor (cyu3imagesnesor.c)
 ## ===========================
 ##
 ##  Copyright Cypress Semiconductor Corporation, 2013,
 ##  All Rights Reserved
 ##  UNPUBLISHED, LICENSED SOFTWARE.
 ##
 ##  CONFIDENTIAL AND PROPRIETARY INFORMATION
 ##  WHICH IS THE PROPERTY OF CYPRESS.
 ##
 ##  Use of this file is governed
 ##  by the license agreement included in the file
 ##
 ##     <install>/license/license.txt
 ##
 ##  where <install> is the Cypress software
 ##  installation root directory path.
 ##
 ## ===========================
 */

#include "cyu3imagesensor.h"
#include <cyu3error.h>
#include <cyu3i2c.h>
#include <cyu3utils.h>
#include <cyu3gpio.h>

#define AR1630_PWDN_GPIO 44

/*Uncomment below line to enable UART debug messages during Sensor configuration*/
#define SENSOR_DEBUG

/* Misc Macros; Fill the correct I2C addresses below */
#define AR1630_I2C_ADRESS               (0x36)
#define AR1630_I2C_READ_ADDRESS         ((AR1630_I2C_ADRESS << 1) | 1)	//0x6D
#define AR1630_I2C_WRITE_ADDRESS        ((AR1630_I2C_ADRESS << 1))		//0x6C
#define AR1630_CHIP_ID                  (0x0550) //1360

/* AR1630 Regiser Addresses */
#define AR1630_VERSION_REG				(0x3000)
#define AR1630_SW_RESET					(0x0103)



/* Structure to store the Address/Value pair for each
 * register to be updated. */
typedef struct CyAr1630Reg
{
    uint16_t regAddr;
    uint16_t regValue;
} CyAr1630Reg_t;

/* Globals */

static CyBool_t glIsValidSensor = CyFalse;

/* Configuration Settings*/

/* Configuration Settings for Sensor Init*/
CyAr1630Reg_t SENSOR_BaseConfigurationSettings [] =
{
/*	//===
	//LOAD= Analog_Setup_Recommended
	{0x3EB4,0x676A},
	{0x3EBC,0x0010},
	{0x3ECE,0x008C},
	{0x3ED0,0x0071},
	{0x3ED2,0x6030},
	{0x3ED4,0x0C0A},
	{0x3ED8,0x9744},
	{0x3EDC,0x7D04},
	{0x30EE,0x1140},
	{0x3120,0x0001},
	{0x3044,0x05E0},
	{0x30C0,0x0004},
	{0x30C2,0x0100},
	{0x3EBA,0x0111},
	{0x3EDA,0xB21D},
	{0x316E,0x8400},
	{0x3EF0,0x0E0E},
	{0x3EF2,0x0103},
	{0x3EAE,0x000E},
	{0x3EBE,0x5504},
	{0x3EC0,0x8888},
	{0x3EC2,0x008B},
	{0x3EC4,0x27A2},
	{0x3EC6,0x913D},
	{0x3ECA,0x90B7},
	{0x3EB8,0x1378},
	{0x3F3A,0x0080},
	//END Analog_Setup_Recommended

	//===
	//LOAD= Pixel_Timing_Recommended_10bit
	{0x3D00,0x046D},
	{0x3D02,0xFF73},
	{0x3D04,0xFFFF},
	{0x3D06,0xFFFF},
	{0x3D08,0xA800},
	{0x3D0A,0x3611},
	{0x3D0C,0x3023},
	{0x3D0E,0xA84B},
	{0x3D10,0x6280},
	{0x3D12,0x3105},
	{0x3D14,0x0808},
	{0x3D16,0x8010},
	{0x3D18,0xC010},
	{0x3D1A,0x0331},
	{0x3D1C,0x0882},
	{0x3D1E,0x5A8C},
	{0x3D20,0x5981},
	{0x3D22,0x7385},
	{0x3D24,0x7380},
	{0x3D26,0x7385},
	{0x3D28,0x1003},
	{0x3D2A,0x8143},
	{0x3D2C,0x8058},
	{0x3D2E,0x1C00},
	{0x3D30,0x8454},
	{0x3D32,0x49EA},
	{0x3D34,0x608C},
	{0x3D36,0x7394},
	{0x3D38,0x4785},
	{0x3D3A,0x6880},
	{0x3D3C,0x6180},
	{0x3D3E,0x4947},
	{0x3D40,0x5968},
	{0x3D42,0x5556},
	{0x3D44,0x8646},
	{0x3D46,0x8873},
	{0x3D48,0x8210},
	{0x3D4A,0x0C8C},
	{0x3D4C,0x6A81},
	{0x3D4E,0x2840},
	{0x3D50,0x9510},
	{0x3D52,0x0C83},
	{0x3D54,0x4689},
	{0x3D56,0x7B86},
	{0x3D58,0x738E},
	{0x3D5A,0x5528},
	{0x3D5C,0x4081},
	{0x3D5E,0x596A},
	{0x3D60,0x8249},
	{0x3D62,0x6B83},
	{0x3D64,0x6B83},
	{0x3D66,0x6D8A},
	{0x3D68,0x7382},
	{0x3D6A,0x73AC},
	{0x3D6C,0x7380},
	{0x3D6E,0x4785},
	{0x3D70,0x6882},
	{0x3D72,0x4947},
	{0x3D74,0x5968},
	{0x3D76,0x558A},
	{0x3D78,0x5880},
	{0x3D7A,0x5781},
	{0x3D7C,0x7380},
	{0x3D7E,0x10F0},
	{0x3D80,0x805A},
	{0x3D82,0x8245},
	{0x3D84,0x8D6A},
	{0x3D86,0x5B28},
	{0x3D88,0x408F},
	{0x3D8A,0x4182},
	{0x3D8C,0x4181},
	{0x3D8E,0x4354},
	{0x3D90,0x8110},
	{0x3D92,0x0381},
	{0x3D94,0x1030},
	{0x3D96,0x8446},
	{0x3D98,0x8210},
	{0x3D9A,0x0C9A},
	{0x3D9C,0x4A86},
	{0x3D9E,0x4A87},
	{0x3DA0,0x7B8B},
	{0x3DA2,0x6783},
	{0x3DA4,0x7387},
	{0x3DA6,0x100C},
	{0x3DA8,0x8146},
	{0x3DAA,0x8110},
	{0x3DAC,0x3081},
	{0x3DAE,0x4311},
	{0x3DB0,0x0341},
	{0x3DB2,0x8100},
	{0x3DB4,0x0A86},
	{0x3DB6,0x1133},
	{0x3DB8,0x8046},
	{0x3DBA,0x8210},
	{0x3DBC,0x0CA6},
	{0x3DBE,0x4A86},
	{0x3DC0,0x4A94},
	{0x3DC2,0x7373},
	{0x3DC4,0x8110},
	{0x3DC6,0x0C73},
	{0x3DC8,0x4611},
	{0x3DCA,0x3380},
	{0x3DCC,0x0018},
	{0x3DCE,0x0006},
	{0x3DD0,0x8155},
	{0x3DD2,0x812C},
	{0x3DD4,0xE06E},
	{0x3DD6,0x8036},
	{0x3DD8,0x4570},
	{0x3DDA,0x8000},
	{0x3DDC,0x0382},
	{0x3DDE,0x4BC3},
	{0x3DE0,0x4B82},
	{0x3DE2,0x0003},
	{0x3DE4,0x8070},
	{0x3DE6,0xFFFF},
	{0x3DE8,0x937B},
	{0x3DEA,0x0000},
	{0x3DEC,0x0000},
	{0x3DEE,0x0000},
	{0x3DF0,0x0000},
	{0x3DF2,0x0000},
	{0x3DF4,0x0000},
	{0x3DF6,0x0000},
	{0x3DF8,0x0000},
	{0x3DFA,0x0000},
	{0x3DFC,0x0000},
	{0x3DFE,0x0000},
	{0x3E00,0x0000},
	{0x3E02,0x0000},
	{0x3E04,0x0000},
	{0x3E06,0x0000},
	{0x3E08,0x0000},
	{0x3E0A,0x0000},
	{0x3E0C,0x0000},
	{0x3E0E,0x0000},
	{0x3E10,0x0000},
	{0x3E12,0x0000},
	{0x3E14,0x0000},
	{0x3E16,0x0000},
	{0x3E18,0x0000},
	{0x3E1A,0x0000},
	{0x3E1C,0x0000},
	{0x3E1E,0x0000},
	{0x3E20,0x0000},
	{0x3E22,0x0000},
	{0x3E24,0x0000},
	{0x3E26,0x0000},
	{0x3E28,0x0000},
	{0x3E2A,0x0000},
	{0x3E2C,0x0000},
	{0x3E2E,0x0000},
	{0x3E30,0x0000},
	{0x3E32,0x0000},
	{0x3E34,0x0000},
	{0x3E36,0x0000},
	{0x3E38,0x0000},
	{0x3E3A,0x0000},
	{0x3E3C,0x0000},
	{0x3E3E,0x0000},
	{0x3E40,0x0000},
	{0x3E42,0x0000},
	{0x3E44,0x0000},
	{0x3E46,0x0000},
	{0x3E48,0x0000},
	{0x3E4A,0x0000},
	{0x3E4C,0x0000},
	{0x3E4E,0x0000},
	{0x3E50,0x0000},
	{0x3E52,0x0000},
	{0x3E54,0x0000},
	{0x3E56,0x0000},
	{0x3E58,0x0000},
	{0x3E5A,0x0000},
	{0x3E5C,0x0000},
	{0x3E5E,0x0000},
	{0x3E60,0x0000},
	{0x3E62,0x0000},
	{0x3E64,0x0000},
	{0x3E66,0x0000},
	{0x3E68,0x0000},
	{0x3E6A,0x0000},
	{0x3E6C,0x0000},
	{0x3E6E,0x0000},
	{0x3E70,0x0000},
	{0x3E72,0x0000},
	{0x3E74,0x0000},
	{0x3E76,0x0000},
	{0x3E78,0x0000},
	{0x3E7A,0x0000},
	{0x3E7C,0x0000},
	{0x3E7E,0x0000},
	{0x3E80,0x0000},
	{0x3E82,0x0000},
	{0x3E84,0x0000},
	{0x3E86,0x0000},
	{0x3E88,0x0000},
	{0x3E8A,0x0000},
	{0x3E8C,0x0000},
	{0x3E8E,0x0000},
	{0x3E90,0x0000},
	{0x3E92,0x0000},
	{0x3E94,0x0000},
	{0x3E96,0x0000},
	{0x3E98,0x0000},
	{0x3E9A,0x0000},
	{0x3E9C,0x0000},
	{0x3E9E,0x0000},
	{0x3EA0,0x0000},
	{0x3EA2,0x0000},
	{0x3EA4,0x0000},
	{0x3EA6,0x0000},
	{0x3EA8,0x0000},
	{0x3EAA,0x0000},
	{0x3EAC,0x0000},*/

	//===
	//LOAD = PLL_880MHz_24MHz
	{0x0300,0x0006},	//VT_PIX_CLK_DIV
	{0x0302,0x0C01},	//VT_SYS_CLK_DIV2
	{0x0304,0x0A02},	//PRE_PLL_CLK_DIV2
	{0x0306,0x204B},	//PLL_MULTIPLIER2
	{0x0308,0x0004},	//OP_PIX_CLK_DIV
	{0x030A,0x0001},	//OP_SYS_CLK_DIV
	{0x030C,0x10AF},	//MIPI_INT_PHY_PLL
	{0x030E,0x0002},	//PLL_CTRL
	//END PLL_880MHz_24MHz

	//===
	//REG = 0x030C, 0x10AE
	{0x0112,0x0A08},	//Set the data format as RAW8(0x0808), RAW10(0x0A0A) and RAW12(0x0C0C)
	{0x31AE,0x0204},	//Set MIPI with 4 lanes

	//===
	//LOAD= MIPI_TIMING_880MHz
	{0x31B0,0x003B},	//Frame preamble
	{0x31B2,0x003B},	//Line preamble
	{0x31B4,0x130A},	//MIPI timing0
	{0x31B6,0x16CA},	//MIPI timing1
	{0x31B8,0x1251},	//MIPI timing2
	{0x31BA,0xB008},	//MIPI timing3
	{0x31BC,0x4D3D},	//MIPI timing4
	//END MIPI_TIMING_880MHz

	//===
	{0x0344,0x0008},	//X_ADDR_START
	{0x0348,0x121F},	//X_ADDR_END
	{0x0346,0x0008},	//Y_ADDR_START
	{0x034A,0x0DAB},	//Y_ADDR_END
	{0x034C,0x1218},	//X_OUTPUT_SIZE
	{0x034E,0x0DA4},	//Y_OUTPUT_SIZE

	{0x0400,0x0000},	//SCALING_MODE
	{0x0404,0x1000},	//Scale_M
	{0x0408,0x1010},	//Second Residual
	{0x040A,0x075D},	//Second Crop

	{0x0342,0x2460},	//LINE_LENGTH_PCK
	{0x0340,0x0DFA},	//FRAME_LENGTH_LINES
	{0x0202,0x0DFA},	//COARSE_INTEGRATION_TIME

	//===
	{0x31D6,0x2B2B}

	//IMAGE= 4632,3492, BAYER-10
};


					
/* Configuration settings for 16M */
CyAr1630Reg_t SENSOR_16MConfigurationSettings [] =
{
	{0x0344,0x0008},	//X_ADDR_START	8
	{0x0348,0x121F},	//X_ADDR_END	4639
	{0x0346,0x0008},	//Y_ADDR_START	8
	{0x034A,0x0DAB},	//Y_ADDR_END	3499
	{0x034C,0x1218},	//X_OUTPUT_SIZE	4632
	{0x034E,0x0DA4}		//Y_OUTPUT_SIZE	3492
};
/* Configuration settings for 4K */
CyAr1630Reg_t SENSOR_4KConfigurationSettings [] =
{
	{0x0344,0x0193},	//X_ADDR_START	8+(4632-3840)/2-1 = 403
	{0x0348,0x1095},	//X_ADDR_END	404+3840+1 = 4245
	{0x0346,0x0299},	//Y_ADDR_START	8+(3492-2160)/2-1 = 665
	{0x034A,0x0B0B},	//Y_ADDR_END	666+2160+1 = 2827
	{0x034C,0x0F00},	//X_OUTPUT_SIZE	3840
	{0x034E,0x0870}		//Y_OUTPUT_SIZE	2160
};
/* Configuration settings for 1080p */
CyAr1630Reg_t SENSOR_1080pConfigurationSettings [] =
{
	{0x0344,0x0553},	//X_ADDR_START	8+(4632-1920)/2-1 = 1363
	{0x0348,0x0CD5},	//X_ADDR_END	1364+1920+1 = 3285
	{0x0346,0x04BD},	//Y_ADDR_START	8+(3492-1080)/2-1 = 1213
	{0x034A,0x08F7},	//Y_ADDR_END	1214+1080+1 = 2295
	{0x034C,0x0780},	//X_OUTPUT_SIZE	1920
	{0x034E,0x0438}		//Y_OUTPUT_SIZE	1080
};
/* Configuration settings for 720p */
CyAr1630Reg_t SENSOR_720pConfigurationSettings [] =
{
	{0x0344,0x0693},	//X_ADDR_START	8+(4632-1280)/2-1 = 1683
	{0x0348,0x0B95},	//X_ADDR_END	1684+1280+1 = 2965
	{0x0346,0x0571},	//Y_ADDR_START	8+(3492-720)/2-1 = 1393
	{0x034A,0x0843},	//Y_ADDR_END	1394+720+1 = 2115
	{0x034C,0x0500},	//X_OUTPUT_SIZE	1280
	{0x034E,0x02D0}		//Y_OUTPUT_SIZE	720
};
/* Configuration settings for Vga */
CyAr1630Reg_t SENSOR_VgaConfigurationSettings [] =
{
	{0x0344,0x07D3},	//X_ADDR_START	8+(4632-640)/2-1 = 2003
	{0x0348,0x0A55},	//X_ADDR_END	2004+640+1 = 2645
	{0x0346,0x05E9},	//Y_ADDR_START	8+(3492-480)/2-1 = 1513
	{0x034A,0x07CB},	//Y_ADDR_END	1514+480+1 = 1995
	{0x034C,0x1218},	//X_OUTPUT_SIZE	640
	{0x034E,0x0DA4}		//Y_OUTPUT_SIZE	480
};

CyU3PReturnStatus_t I2C_SensorRead(uint16_t regAddr, uint8_t count, uint8_t *buf)
{
    CyU3PReturnStatus_t status = CY_U3P_SUCCESS;
    CyU3PI2cPreamble_t preamble;
    uint8_t cnt=0;

    for(cnt=0; cnt<3 ; cnt++)
    {
        preamble.buffer[1] = CY_U3P_GET_MSB (regAddr);
        preamble.buffer[2] = CY_U3P_GET_LSB (regAddr);
        preamble.buffer[3] = AR1630_I2C_READ_ADDRESS; /* Slave address: Read operation */
        preamble.buffer[0] = AR1630_I2C_WRITE_ADDRESS; /* Slave address: write operation */
        preamble.length = 4;
        preamble.ctrlMask = 0x0004;

        status = CyU3PI2cReceiveBytes (&preamble, buf, count,0);
        CyU3PThreadSleep(1);
        if (status == CY_U3P_SUCCESS)
        {
            break;
        }
#ifdef SENSOR_DEBUG
        else
            CyU3PDebugPrint(4,"\r\nImageSensorSensorRead Failed addr=0x%x",regAddr);
#endif
    }
    return status;
}

CyU3PReturnStatus_t I2C_SensorWrite(uint16_t regAddr, uint16_t count, uint8_t *buf)
{
    CyU3PReturnStatus_t status = CY_U3P_SUCCESS;
    CyU3PI2cPreamble_t preamble;
    uint8_t cnt=0;

    for(cnt=0; cnt<3 ; cnt++)
    {
        preamble.buffer[1] = CY_U3P_GET_MSB (regAddr);
        preamble.buffer[2] = CY_U3P_GET_LSB (regAddr);
        preamble.buffer[0] = AR1630_I2C_WRITE_ADDRESS; /* Slave address: write operation */
        preamble.length = 3;
        preamble.ctrlMask = 0x0000;

        status = CyU3PI2cTransmitBytes (&preamble, buf, count,0);
        CyU3PThreadSleep(1);
        if (status == CY_U3P_SUCCESS)
        {
            break;
        }
#ifdef SENSOR_DEBUG
        else
            CyU3PDebugPrint(4,"\r\nImageSensorSensorWrite Failed addr=0x%x", regAddr);
#endif
    }
    return status;
}

CyU3PReturnStatus_t SENSOR_Streaming(uint8_t stream)
{
#ifdef SENSOR_DEBUG
    CyU3PDebugPrint(4,"\r\n>>SENSOR_Streaming = %d<<\r\n", stream);
#endif
	CyU3PReturnStatus_t status = CY_U3P_SUCCESS;
	uint8_t readBuffer [2];

    status = I2C_SensorRead(0x301A, 2, readBuffer);
    if (status != CY_U3P_SUCCESS)
        return status;

    if (stream == 0){
        readBuffer[1] = readBuffer[1] & ~(1<<2); // Clear the third bit
    }else{
        readBuffer[1] = readBuffer[1] | 1<<2; // Clear the third bit
    }
    status = I2C_SensorWrite(0x301A, 2, readBuffer);
    CyU3PBusyWait (1000); //wait 1ms
    if (status != CY_U3P_SUCCESS)
        return status;
}

/* Function to verify that the image sensor is the AR1630 chip
 * Sets up flag glIsValidSensor which is to be verified before
 * any other public calls are executed*/
CyU3PReturnStatus_t SENSOR_VerifyChipId()
{
    uint8_t readBuffer [2];

    CyU3PReturnStatus_t status = CY_U3P_SUCCESS;

    readBuffer [0] = readBuffer [1] = 0;
    status = I2C_SensorRead(AR1630_VERSION_REG, 2, readBuffer);
    if (status != CY_U3P_SUCCESS)
        return status;

    if (AR1630_CHIP_ID != CY_U3P_MAKEWORD(readBuffer[0], readBuffer[1])){
        status = CY_U3P_ERROR_BAD_ARGUMENT;
        glIsValidSensor = CyFalse;
    }
    else{
        glIsValidSensor = CyTrue;
    }

#ifdef SENSOR_DEBUG
    CyU3PDebugPrint(4,"Chip ID Read = %d\r\n", CY_U3P_MAKEWORD(readBuffer[0], readBuffer[1]));
#endif

    status = I2C_SensorRead(0x31FE, 2, readBuffer); // Customer Revision Number
    if (status != CY_U3P_SUCCESS)
        return status;

#ifdef SENSOR_DEBUG
    CyU3PDebugPrint(4,"Revision Number = %d\r\n", CY_U3P_MAKEWORD(readBuffer[0], readBuffer[1]) & 0x000F);
#endif

    return status;
}

/*Use the below function to write to 16-bit register */
CyU3PReturnStatus_t SENSOR_WriteConfigurationSettings_Reg16 (CyAr1630Reg_t * configSettings, uint16_t configSettingsSize)
{
    uint16_t regCounter = 0;
    CyU3PReturnStatus_t status = CY_U3P_SUCCESS;
    uint8_t rwBuffer[2];
#ifdef SENSOR_DEBUG
    CyU3PDebugPrint (4,"Writing Configuration Settings:\r\n");
#endif

    for (regCounter = 0; ((regCounter < configSettingsSize) && (status == CY_U3P_SUCCESS)); regCounter++)
    {
#ifdef SENSOR_DEBUG
        CyU3PDebugPrint (4,"Register = 0x%x: Value = 0x%x\r\n",configSettings[regCounter].regAddr,
                configSettings[regCounter].regValue);
#endif
        rwBuffer[0] = (((configSettings[regCounter].regValue)>>8)&0xFF);
		rwBuffer[1] = ((configSettings[regCounter].regValue)&0xFF);
        
        status = I2C_SensorWrite (configSettings[regCounter].regAddr, 2, &rwBuffer); 
    }

    return status;
}

/* Configure AR1630 for 1080P @30FPS*/
CyU3PReturnStatus_t CyCx3_ImageSensor_Set_1080p(void)
{
#ifdef SENSOR_DEBUG
    CyU3PDebugPrint(4,"\r\n>>CyCx3_ImageSensor_Set_1080p<<\r\n");
#endif
    uint16_t configSize;
    if (glIsValidSensor != CyTrue)
        return CY_U3P_ERROR_NOT_SUPPORTED;

    configSize = (sizeof(SENSOR_1080pConfigurationSettings))/(sizeof(CyAr1630Reg_t));
    return SENSOR_WriteConfigurationSettings_Reg16(SENSOR_1080pConfigurationSettings, configSize);
}

/* Configure AR1630 for 720P @60FPS*/
CyU3PReturnStatus_t CyCx3_ImageSensor_Set_720p(void)
{
#ifdef SENSOR_DEBUG
    CyU3PDebugPrint(4,"\r\n>>CyCx3_ImageSensor_Set_720p<<\r\n");
#endif
	uint16_t configSize;
    if (glIsValidSensor != CyTrue)
        return CY_U3P_ERROR_NOT_SUPPORTED;

    configSize = (sizeof(SENSOR_720pConfigurationSettings))/(sizeof(CyAr1630Reg_t));
    return SENSOR_WriteConfigurationSettings_Reg16(SENSOR_720pConfigurationSettings, configSize);
}

/* Configure AR1630 for VGA @60FPS*/
CyU3PReturnStatus_t CyCx3_ImageSensor_Set_VGA(void)
{
#ifdef SENSOR_DEBUG
    CyU3PDebugPrint(4,"\r\n>>CyCx3_ImageSensor_Set_Vga<<\r\n");
#endif
    uint16_t configSize;
    if (glIsValidSensor != CyTrue)
        return CY_U3P_ERROR_NOT_SUPPORTED;

    configSize = (sizeof(SENSOR_VgaConfigurationSettings))/(sizeof(CyAr1630Reg_t));
    return SENSOR_WriteConfigurationSettings_Reg16(SENSOR_VgaConfigurationSettings, configSize);
}

/* Configure AR1630 for VGA @60FPS*/
CyU3PReturnStatus_t SENSOR_ImageSensor_Set_Base()
{
#ifdef SENSOR_DEBUG
    CyU3PDebugPrint(4,"\r\n>>SENSOR_ImageSensor_Set_Base<<\r\n");
#endif
    uint16_t configSize;
    if (glIsValidSensor != CyTrue)
        return CY_U3P_ERROR_NOT_SUPPORTED;

#ifdef SENSOR_DEBUG
    CyU3PDebugPrint(4,"\r\n>>Basic Config<<\r\n");
#endif

	configSize = (sizeof(SENSOR_BaseConfigurationSettings))/(sizeof(CyAr1630Reg_t));
    return SENSOR_WriteConfigurationSettings_Reg16(SENSOR_BaseConfigurationSettings, configSize);
}

/* Configure AR1630 for 4K () @15FPS*/
CyU3PReturnStatus_t CyCx3_ImageSensor_Set_4K(void)
{
#ifdef SENSOR_DEBUG
    CyU3PDebugPrint(4,"\r\n>>CyCx3_ImageSensor_Set_4K<<\r\n");
#endif
    uint16_t configSize;
    if (glIsValidSensor != CyTrue)
        return CY_U3P_ERROR_NOT_SUPPORTED;

    configSize = (sizeof(SENSOR_4KConfigurationSettings))/(sizeof(CyAr1630Reg_t));
    return SENSOR_WriteConfigurationSettings_Reg16(SENSOR_4KConfigurationSettings, configSize);
}

/* Configure Sensor for 16M*/
CyU3PReturnStatus_t CyCx3_ImageSensor_Set_16M(void)
{
#ifdef SENSOR_DEBUG
    CyU3PDebugPrint(4,"\r\n>>CyCx3_ImageSensor_Set_16M<<\r\n");
#endif
    uint16_t configSize;
    if (glIsValidSensor != CyTrue)
        return CY_U3P_ERROR_NOT_SUPPORTED;

    configSize = (sizeof(SENSOR_16MConfigurationSettings))/(sizeof(CyAr1630Reg_t));
    return SENSOR_WriteConfigurationSettings_16M(SENSOR_16MConfigurationSettings, configSize);
}

/* Hardware Reset on the Image Sensor*/
CyU3PReturnStatus_t CyCx3_ImageSensor_ResetHW()
{
    if (glIsValidSensor != CyTrue)
        return CY_U3P_ERROR_NOT_SUPPORTED;

	CyU3PReturnStatus_t apiRetStatus;
	apiRetStatus =  CyU3PGpioSetValue(AR1630_PWDN_GPIO, 1); //High level
    if (apiRetStatus == 0){
    	apiRetStatus =  CyU3PGpioSetValue(AR1630_PWDN_GPIO, 2); //Low level
    	CyU3PThreadSleep (50000); //wait 50ms
        if (apiRetStatus == 0){
        	apiRetStatus =  CyU3PGpioSetValue(AR1630_PWDN_GPIO, 1); //High level
        	CyU3PThreadSleep (50000); //wait 50ms
            return apiRetStatus;
        }
    }
    CyU3PDebugPrint (4, "CyU3PGpioSetIoMode failed, error code = %d\n", apiRetStatus);
    return apiRetStatus;
}

/* Software Reset on the Image Sensor*/
CyU3PReturnStatus_t CyCx3_ImageSensor_ResetSW()
{
#ifdef SENSOR_DEBUG
    CyU3PDebugPrint(4,"\r\n>>CyCx3_ImageSensor_ResetSW<<\r\n");
#endif
    uint8_t writeBuffer = 1;
    CyU3PReturnStatus_t status = I2C_SensorWrite(AR1630_SW_RESET, sizeof(writeBuffer), &writeBuffer);
    if (status != CY_U3P_SUCCESS)
        return status;

#ifdef SENSOR_DEBUG
    CyU3PDebugPrint(4,"\r\nError on Software Reset\n");
#endif
    return status;
}

/*Use the below function to initialize Image Sensor*/
CyU3PReturnStatus_t CyCx3_ImageSensor_Init()
{
	//CyCx3_ImageSensor_ResetHW();

#ifdef SENSOR_DEBUG
    CyU3PDebugPrint(4,"\r\n>>Sensor Initialization<<\r\n");
#endif

    CyU3PReturnStatus_t status = CY_U3P_SUCCESS;

    status = SENSOR_Streaming(0);
    if (status != CY_U3P_SUCCESS)
        return status;

    status = SENSOR_VerifyChipId();
    if (status != CY_U3P_SUCCESS)
        return status;

    status = SENSOR_ImageSensor_Set_Base ();
    if (status != CY_U3P_SUCCESS)
        return status;

    status = CyCx3_ImageSensor_Set_4K ();
    if (status != CY_U3P_SUCCESS)
        return status;

    status = SENSOR_Streaming(1);
    if (status != CY_U3P_SUCCESS)
        return status;

    CyU3PBusyWait (50000); //wait 50ms

    return status;
}

/*Use the below function to put Image Sensor to Sleep*/
CyU3PReturnStatus_t CyCx3_ImageSensor_Sleep()
{
#ifdef SENSOR_DEBUG
    CyU3PDebugPrint(4,"\r\n>>CyCx3_ImageSensor_Sleep<<\r\n");
#endif
    CyU3PReturnStatus_t status = CY_U3P_SUCCESS;
    status = SENSOR_Streaming(0);
    if (status != CY_U3P_SUCCESS)
        return status;
}

/*Use the below function to wakeup Image Sensor*/
CyU3PReturnStatus_t CyCx3_ImageSensor_Wakeup()
{
#ifdef SENSOR_DEBUG
    CyU3PDebugPrint(4,"\r\n>>CyCx3_ImageSensor_Wakeup<<\r\n");
#endif
    CyU3PReturnStatus_t status = CY_U3P_SUCCESS;
    status = SENSOR_Streaming(1);
    if (status != CY_U3P_SUCCESS)
        return status;
}

CyU3PReturnStatus_t CyCx3_ImageSensor_Trigger_Autofocus()
{
#ifdef SENSOR_DEBUG
    CyU3PDebugPrint(4,"\r\n>>CyCx3_ImageSensor_Trigger_Autofocus<<\r\n");
#endif
	CyU3PReturnStatus_t status = CY_U3P_SUCCESS;
	return status;
}

CyU3PReturnStatus_t CyCx3_ImageSensor_Set_Format(CyU3PSensorStreamFormat_t format)
{
#ifdef SENSOR_DEBUG
    CyU3PDebugPrint(4,"\r\n>>CyCx3_ImageSensor_Set_Format<<\r\n");
#endif
    CyU3PReturnStatus_t status = CY_U3P_SUCCESS;
    return status;
}


